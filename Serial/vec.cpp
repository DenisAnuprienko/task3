#include "header.h"

Vec::Vec(Partition *P)
{
    Prt = P;

    M = Prt->_M();
    N = Prt->_N();

    hx = (A2 - A1) / M;
    hy = (B2 - B1) / N;

    H = Prt->_Height() + 2;
    W = Prt->_Width()  + 2;
    size = (size_t)(H * W);

    #ifdef USE_GPU
    hvec.resize(size);
    for(int i = 0; i < H*W; i++)
        hvec[i] = 0.0;
    #else
    vals = (double *)malloc(size * sizeof(double));
    for(int i = 0; i < size; i++)
        vals[i] = 0.0;
    #endif

    sendbuf_upper = (double *)malloc(Prt->_Width() * sizeof(double));
    sendbuf_lower = (double *)malloc(Prt->_Width() * sizeof(double));
    recvbuf_upper = (double *)malloc(Prt->_Width() * sizeof(double));
    recvbuf_lower = (double *)malloc(Prt->_Width() * sizeof(double));

    sendbuf_left  = (double *)malloc(Prt->_Height() * sizeof(double));
    sendbuf_right = (double *)malloc(Prt->_Height() * sizeof(double));
    recvbuf_left  = (double *)malloc(Prt->_Height() * sizeof(double));
    recvbuf_right = (double *)malloc(Prt->_Height() * sizeof(double));
}

Vec::~Vec()
{
    #ifndef USE_GPU
    free(vals);
    #endif
    free(sendbuf_upper);
    free(sendbuf_lower);
    free(recvbuf_upper);
    free(recvbuf_lower);
    free(sendbuf_left);
    free(sendbuf_right);
    free(recvbuf_left);
    free(recvbuf_right);
}


double Vec::DotProduct(Vec & w)
{
    double prod = 0e0;

    assert(Prt->_taskid() == w.getPrt()->_taskid());
    assert(size == w.size);


    for(int i = 0; i < W-2; i++)
        for(int j = 0; j < H-2; j++)
            prod += (*this)(i,j) * w(i,j);

    return prod;
}

double Vec::Norm2(void)
{
    double norm = 0.0;

    for(int i = 0; i < W-2; i++)
        for(int j = 0; j < H-2; j++)
            norm += (*this)(i,j) * (*this)(i,j);


    return sqrt(norm);
}

void Vec::MatVec(Vec & w)
{
    // if(Prt->_taskid() == MASTER)
    //     printf("MatVec\n");
    //Prt->SyncVec(w);
    // time_t t1, t2;
    // t1 = Prt->Time();
    // t2 = Prt->Time();
    // Prt->AddTime(t2-t1, T_SYNC);

    // i and j are LOCAL indices for Vec, which is only
    // a subvector related to a subdomain
    // I and J are GLOBAL mesh indices
    int I, J;
    int imin = Prt->_imin(), jmin = Prt->_jmin();
    //printf("MatVec: %d %d\n", W-2, H-2);

    for(int i = 0; i < W-2; i++){
        I = i + imin;
        for(int j = 0; j < H-2; j++){
    //  for(j = jmin; j <= jmax; j++){
    //      for(i = imin; i <= imax; i++){
            J = j + jmin;
            
            if(I == 0){ // Left side including corners
                if(J == 0){ // Left lower corner
                    //printf("L corner\n");
                    (*this)(i,j) = -2.0 * (w(i+1,j) - w(i,j)) / (hx*hx) - 2.0 * (w(i,j+1) - w(i,j)) / (hy*hy);
                }
                else if(J == N){ // Left upper corner
                    //printf("U corner\n");
                    (*this)(i,j) = -2.0 * (w(i+1,j) - w(i,j)) / (hx*hx) + 2.0 * (w(i,j) - w(i,j-1)) / (hy*hy);
                }
                else{ // Left side excluding corners
                    //printf("L\n");
                    (*this)(i,j) = -2.0 * (w(i+1,j) - w(i,j)) / (hx*hx) - (w(i,j+1) - 2.0*w(i,j) + w(i,j-1)) / (hy*hy);
                }
            }
            else{
                if(J == 0 && I != 0){ // Bottom side
                     //printf("B\n");
                     if(I < M-1)
                         (*this)(i,j) = -(w(i-1,j) - 2.0*w(i,j) + w(i+1,j)) / (hx*hx) - 2.0 * (w(i,j+1) - w(i,j)) / (hy*hy);
                     else{
                         //printf("(%d,%d): upr = %d\n", i, j, upr);
                         (*this)(i,j) = -(w(i-1,j) - 2.0*w(i,j)         ) / (hx*hx) - 2.0 * (w(i,j+1) - w(i,j)) / (hy*hy);
                     }
                }
                else if(J == N && I != 0){ // Top side
                     //printf("T\n");
                     if(I < M-1)
                         (*this)(i,j) = -(w(i-1,j) - 2.0*w(i,j) + w(i+1,j)) / (hx*hx) + 2.0 * (w(i,j) - w(i,j-1)) / (hy*hy);
                     else
                         (*this)(i,j) = -(w(i-1,j) - 2.0*w(i,j)           ) / (hx*hx) + 2.0 * (w(i,j) - w(i,j-1)) / (hy*hy);
                }
                else{ // Interior points
                     if(I < M-1)
                         (*this)(i,j) = -(w(i-1,j) - 2.0*w(i,j) + w(i+1,j)) / (hx*hx) - (w(i,j+1) - 2.0*w(i,j) + w(i,j-1)) / (hy*hy);
                     else
                         (*this)(i,j) = -(w(i-1,j) - 2.0*w(i,j)         ) / (hx*hx) - (w(i,j+1) - 2.0*w(i,j) + w(i,j-1)) / (hy*hy);
                }
            }
        }
    }
    // if(Prt->_taskid() == MASTER)
    //     printf("MatVec finished\n");
}   

void Vec::FormRHS(void)
{
    int I, J;
    int imin = Prt->_imin(), jmin = Prt->_jmin();


    for(int i = 0; i < W-2; i++){
        I = i + imin;
        for(int j = 0; j < H-2; j++){
            J = j + jmin;

            (*this)(i,j) = f(A1 + I*hx, B1 + J*hy);
            
            // Right boundary (Dirichlet) influence
            if(I == M-1){
               //printf("Right boundary\n");
               (*this)(i,j) += 1.0 / (hx*hx) * ExactSol(A2, B1 + J*hy);
            }
            
            //continue;//////////////////////////////////////////////////////////////////////////////////////
            
            if(I == 0){ // Left boundary
                if(J == 0){ // Left lower corner
                    (*this)(i,j) -= (2.0 / hx * BC_Left(B1) + 2.0 / hy * BC_Bottom(A1));// * BC_Corner(0);
                }
                else if(J == N){ // Left upper corner
                    (*this)(i,j) -= (2.0 / hx * BC_Left(B2) + 2.0 / hy * BC_Top(A1));// * BC_Corner(1);
                }
                else{ // The rest
                    (*this)(i,j) -= 2.0 / hx * BC_Left(B1 + J*hy);
                }
            }
            else if(J == 0 && I != 0){ // Bottom boundary excluding corner
               (*this)(i,j) -= 2.0 / hy * BC_Bottom(A1 + I*hx);
            }
            else if(J == N && I != 0){ // Upper boundary excluding corner
               (*this)(i,j) -= 2.0 / hy * BC_Top(A1 + I*hx);
            }
        }
    }
    //printf("FormRHS() completed\n");
}

void Vec::AddVec(Vec & w, double alpha)
{
    for(int i = 0; i < W-2; i++)
        for(int j = 0; j < H-2; j++)
            (*this)(i,j) += alpha*w(i,j);
}

void Vec::ComputeResidual(Vec & x, Vec & b)
{
    time_t t1, t2;
    t1 = Prt->Time();
    #ifdef USE_GPU
    GPU_MatVec(x);
    #else
    MatVec(x);
    #endif
    t2 = Prt->Time();
    Prt->AddTime(t2-t1, T_MATVEC);

    // for(int i = 0; i < W-2; i++)
    //     for(int j = 0; j < H-2; j++)
    //         (*this)(i,j) -= b(i,j);
    t1 = Prt->Time();
    #ifdef USE_GPU
    GPU_AddVec(b, -1.0);
    #else
    AddVec(b, -1.0);
    #endif
    t2 = Prt->Time();
    Prt->AddTime(t2-t1, T_ADD);
}

void Vec::GatherTo(double *g)
{
    int i, j;
    int _W = Prt->_Width(), _H = Prt->_Height();

    printf("task %d reached GatherTo\n", Prt->_taskid());

    for(i = 0; i < _W; i++){
        for(j = 0; j < _H; j++){
            g[i*(N+1) + j] = (*this)(i,j);
        }
    }
}