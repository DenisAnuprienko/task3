#include "header.h"

struct square 
{ 
    __device__ __host__ double operator()(double x) { return x*x; } 
};

struct addvec
{
    double a;
    addvec(double d) : a(d) {}
    __host__ __device__
    double operator()(double x, double y)
    {
        return x + a*y;
    }
};

struct matvec : public thrust::binary_function<double, int, double>
    {
        int imin, jmin, M, N;
        size_t H, W;
        double hx, hy;
        const double *v;
        matvec(thrust::device_vector<double> & vec, int imin_, int jmin_, int H_, int W_, int M_, int N_, double hX, double hY) : 
               v(thrust::raw_pointer_cast(vec.data())), 
               imin(imin_), jmin(jmin_), H(H_), W(W_), M(M_), N(N_), hx(hX), hy(hY)
        {
        }
        __host__ __device__
        double operator()(const double & vij, const int & k) const
        {
            int i = k/H;                     // indices in vec
            int j = k - i*H;                 //
            // return hx;
            // i--;
            // j--;
            int I = i + imin - 1;                // indices in global domain
            int J = j + jmin - 1;                // 
            //printf("%2d (%2d,%2d)\n", k, I, J);

            double lft = 0.0, rgt = 0.0, upr = 0.0, lwr = 0.0;
            if(i > 0)
            //if(I > 0)
                lft = v[(i-1)*H + j];
            if(i < W-1)
            //if(I < M) 
                rgt = v[(i+1)*H + j];
            if(j < H-1)
            //if(J < N+1)
                upr = v[i*H + j+1];
            if(j > 0)
            //if(J > 0)
                lwr = v[i*H + j-1];

            double res;

            if(i == 0 || j == 0 || j == H-1 || i == W-1) {
                //printf(" Ghost\n");
                return 0e0;
                //res = 0e0;
            }

            
            if(I == 0){ // Left side including corners
                if(J == 0){ // Left lower corner
                    //printf("L corner\n");
                    res = -2.0 * (rgt - vij) / (hx*hx) - 2.0 * (upr - vij) / (hy*hy);
                }
                else if(J == N){ // Left upper corner
                    //printf("U corner\n");
                    res = -2.0 * (rgt - vij) / (hx*hx) + 2.0 * (vij - lwr) / (hy*hy);
                }
                else{ // Left side excluding corners
                    //printf("L\n");
                    res = -2.0 * (rgt - vij) / (hx*hx) - (upr - 2.0*vij + lwr) / (hy*hy);
                }
            }
            else{
                if(J == 0 && I != 0){ // Bottom side
                     //printf("B\n");
                     if(I < M-1)
                         res = -(lft - 2.0*vij + rgt) / (hx*hx) - 2.0 * (upr - vij) / (hy*hy);
                     else{
                         //printf("(%d,%d): upr = %d\n", i, j, upr);
                         res = -(lft - 2.0*vij         ) / (hx*hx) - 2.0 * (upr - vij) / (hy*hy);
                     }
                }
                else if(J == N && I != 0){ // Top side
                     //printf("T\n");
                     if(I < M-1)
                         res = -(lft - 2.0*vij + rgt) / (hx*hx) + 2.0 * (vij - lwr) / (hy*hy);
                     else
                         res = -(lft - 2.0*vij           ) / (hx*hx) + 2.0 * (vij - lwr) / (hy*hy);
                }
                else{ // Interior points
                     if(I < M-1)
                         res = -(lft - 2.0*vij + rgt) / (hx*hx) - (upr - 2.0*vij + lwr) / (hy*hy);
                     else
                         res = -(lft - 2.0*vij         ) / (hx*hx) - (upr - 2.0*vij + lwr) / (hy*hy);
                }
            }
            return res;
        }

    };


double Vec::GPU_Norm2(void)
{
	//printf("GPU_Norm2()\n");

     // Make values at ghost points zeros
    for(int i = 0; i < W; i++){
    	hvec[i*H] = 0.0;
    	hvec[(W-1)*H] = 0.0;
    }
    for(int j = 0; j < H; j++){
    	hvec[j] = 0.0;
    	hvec[(W-1)*H + j] = 0.0;
    }

    dvec = hvec;

    //printf(" Copied\n");

    size_t N = dvec.size();
    thrust::device_vector<double> x_squared(N);
    //printf(" Created vec\n");
    thrust::transform(dvec.begin(), dvec.end(), x_squared.begin(), square());
    //printf(" transformed\n");
    double res = thrust::reduce(x_squared.begin(), x_squared.end());
    //printf(" res = %lf\n", res);
    return sqrt(res);
}

double Vec::GPU_DotProduct(Vec & w)
{
    //printf("GPU_Norm2()\n");

     // Make values at ghost points zeros
    for(int i = 0; i < W; i++){
        hvec[i*H] = 0.0;
        hvec[(W-1)*H] = 0.0;
        w.hvec[i*H] = 0.0;
        w.hvec[(W-1)*H] = 0.0;
    }
    for(int j = 0; j < H; j++){
        hvec[j] = 0.0;
        hvec[(W-1)*H + j] = 0.0;
        w.hvec[j] = 0.0;
        w.hvec[(W-1)*H + j] = 0.0;
    }

    dvec = hvec;
    w.dvec = w.hvec;

    //printf(" Copied\n");

    return thrust::inner_product(dvec.begin(), dvec.end(), w.dvec.begin(), 0.0);
}

void Vec::GPU_MatVec(Vec & w)
{
    //printf("GPU_MatVec\n");
    dvec = hvec;
    w.dvec = w.hvec;

    //printf("hx = %lf, hy = %lf\n", hx, hy);
    //printf("H = %d, W = %d\n", H, W);

    struct matvec mv(w.dvec, Prt->_imin(), Prt->_jmin(), H, W, M, N, hx, hy);
    //printf("Created matvec\n");
    thrust::transform(w.dvec.begin(), w.dvec.end(), 
                      thrust::make_counting_iterator(0), 
                      dvec.begin(),
                      mv);
    //printf("transform success\n");
    hvec = dvec;
}

void Vec::GPU_AddVec(Vec & w, double alpha)
{
    //printf("GPU_MatVec\n");
    dvec = hvec;
    w.dvec = w.hvec;

    //printf("hx = %lf, hy = %lf\n", hx, hy);
    //printf("H = %d, W = %d\n", H, W);

    struct addvec s(alpha);
    //printf("Created matvec\n");
    thrust::transform(dvec.begin(), dvec.end(),  
                      w.dvec.begin(),
                      dvec.begin(),
                      s);
    //printf("transform success\n");
    hvec = dvec;
}
