#include "header.h"

Partition::Partition(int *argc_, char ***argv_, int m, int n)
{
    M = m;
    N = n;

    times[T_NORM2]  = 0.0;
    times[T_SYNC]   = 0.0;
    times[T_ADD]    = 0.0;
    times[T_MATVEC] = 0.0;
    times[T_DOT]    = 0.0;


    int dims[2] = {0, 0};


    proc_x = 1;
    proc_y = 1;

    taskid = 0;
    numtasks = 1;

    if (taskid == MASTER){
        //printf("M = %d, N = %d, atol = %e, rtol = %e, maxit = %d\n", M, N, tol_a, tol_r, maxit);
        printf("M = %d, N = %d\n", M, N);
        printf("Number of MPI tasks:      %d\n", numtasks);
        printf("Grid: %d = %d * %d\n", numtasks, proc_x, proc_y);
        //printf("hx = %lf, hy = %lf\n", hx, hy);
    }

    int periods[2] = {0, 0};

    Neighbors();
    Irange();
    Jrange();

    //idx = taskid/proc_y;
    //idy = taskid - idx*proc_y;
}

Partition::~Partition()
{
}

void Partition::Neighbors(void)
{
    Left  = -1;
    Right = -1;
    Upper = -1;
    Lower = -1;
}

void Partition::Irange(void)
{
    imin = 0;
    imax = M-1;
    Width = M;}

void Partition::Jrange(void)
{
    jmin = 0;
    jmax = N;
    Height = N+1;
}


void Partition::Print_solution_VTK(double *x, const char *filename)
{
    int i, j;

    double hx = (A2 - A1) / M;
    double hy = (B2 - B1) / N;

    if(taskid != MASTER)
        return;
    
    FILE *f = fopen(filename, "w");
    fprintf(f, "# vtk DataFile Version 3.0\n");
    fprintf(f, "\n");
    fprintf(f, "ASCII\n");
    fprintf(f, "DATASET RECTILINEAR_GRID\n");
    fprintf(f, "DIMENSIONS %d %d 1\n", M+1, N+1);
    
    
    fprintf(f, "X_COORDINATES %d double\n", M+1);
    for(i = 0; i <= M; i++)
        fprintf(f, "%lf\n", A1 + i*hx);
    fprintf(f, "\n");
    
    fprintf(f, "Y_COORDINATES %d double\n", N+1);
    for(i = 0; i <= N; i ++)
        fprintf(f, "%lf\n", B1 + i*hy);
    fprintf(f, "\n");
    
    fprintf(f, "Z_COORDINATES 1 double\n0\n\n");
    
    fprintf(f, "POINT_DATA %d\n", (M+1)*(N+1));
        
    fprintf(f, "SCALARS Numerical double\n"); 
    fprintf(f, "LOOKUP_TABLE default\n");
    for(j = 0; j <= N; j++){
    //for(i = 0; i <= M-1; i++){
        for(i = 0; i <= M-1; i++){
        //for(j = 0; j <= N; j++){
            fprintf(f, "%lf\n", x[i*(N+1)+j]);
        }
        fprintf(f, "%lf\n", ExactSol(A2, B1 + j*hy));
        //fprintf(f, "\n");
    }
    
    fprintf(f, "\nSCALARS Exact double\n");
    fprintf(f, "LOOKUP_TABLE default\n");
    for(j = 0; j <= N; j ++){
    //for(i = 0; i <= M-1; i ++){
        //for(j = 0; j <= N; j ++){
        for(i = 0; i <= M; i ++){
            fprintf(f, "%lf ", ExactSol(A1 + i*hx, B1 + j*hy));
        }
        fprintf(f, "\n");
    }
    printf("Finished printing solution\n");

    fclose(f);
}

// void Partition::Print_solution_VTK(Vec &x, Vec &procid, const char *filename)
// {
//     int i, j;

//     double hx = (A2 - A1) / M;
//     double hy = (B2 - B1) / N;

//     //GatherVec(x);
//     //GatherVec(procid);
//     MPI_Barrier(Comm);
//     if(taskid != MASTER)
//         return;
    
//     FILE *f = fopen(filename, "w");
//     fprintf(f, "# vtk DataFile Version 3.0\n");
//     fprintf(f, "\n");
//     fprintf(f, "ASCII\n");
//     fprintf(f, "DATASET RECTILINEAR_GRID\n");
//     fprintf(f, "DIMENSIONS %d %d 1\n", M+1, N+1);
    
    
//     fprintf(f, "X_COORDINATES %d double\n", M+1);
//     for(i = 0; i <= M; i++)
//         fprintf(f, "%lf\n", A1 + i*hx);
//     fprintf(f, "\n");
    
//     fprintf(f, "Y_COORDINATES %d double\n", N+1);
//     for(i = 0; i <= N; i ++)
//         fprintf(f, "%lf\n", i*hy);
//     fprintf(f, "\n");
    
//     fprintf(f, "Z_COORDINATES 1 double\n0\n\n");
    
//     fprintf(f, "POINT_DATA %d\n", (M+1)*(N+1));
        
//     fprintf(f, "SCALARS Numerical double\n"); 
//     fprintf(f, "LOOKUP_TABLE default\n");
//     for(j = 0; j <= N; j++){
//     //for(i = 0; i <= M-1; i++){
//         for(i = 0; i <= M-1; i++){
//         //for(j = 0; j <= N; j++){
//             fprintf(f, "%lf\n", x(i, j));//x[i*(N+1)+j]);
//         }
//         fprintf(f, "%lf\n", ExactSol(A2, B1 + j*hy));
//         //fprintf(f, "\n");
//     }
//     //for(j = 0; j <= N; j++)
//     //    fprintf(f, "%lf\n", ExactSol(A2, B1 + j*hy));

//     //for(i = 0; i < (M+1)*(N+1); i++)
//     //    fprintf(f, "%d\n", i);
    
//     fprintf(f, "\nSCALARS Exact double\n");
//     fprintf(f, "LOOKUP_TABLE default\n");
//     for(j = 0; j <= N; j ++){
//     //for(i = 0; i <= M-1; i ++){
//         //for(j = 0; j <= N; j ++){
//         for(i = 0; i <= M; i ++){
//             fprintf(f, "%lf ", ExactSol(A1 + i*hx, B1 + j*hy));
//         }
//         fprintf(f, "\n");
//     }

//     fprintf(f, "SCALARS TaskId double\n"); 
//     fprintf(f, "LOOKUP_TABLE default\n");
//     for(j = 0; j <= N; j++){
//     //for(i = 0; i <= M-1; i++){
//         for(i = 0; i <= M-1; i++){
//         //for(j = 0; j <= N; j++){
//             fprintf(f, "%lf\n", procid(i,j));//procid[i*(N+1)+j]);
//         }
//         fprintf(f, "-1\n");
//         //fprintf(f, "\n");
//     }
//     printf("Finished printing solution\n");

//     fclose(f);
// }


void Partition::CheckError(double *x)
{
    double newerr, err = 0.0;
    int i, j;

    double hx = (A2 - A1) / M;
    double hy = (B2 - B1) / N;

    if(taskid != MASTER)
        return;

    for(i = 0; i <= M-1; i++){
        for(j = 0; j <= N; j++){
            newerr =  fabs(x[i*(N+1)+j] - ExactSol(A1 + i*hx, B1 + j*hy));
            if(newerr > err)
                err = newerr;
        }
    }
    printf("Max error: %e\n", err); 
}

void Partition::PrintTimes(void)
{
    if(taskid != MASTER)
        return;

    printf("+-------------------------\n");
    printf("| T_norm   = %lf\n", times[T_NORM2]);
    printf("| T_dot    = %lf\n", times[T_DOT]);
    printf("| T_matvec = %lf\n", times[T_MATVEC]);
    printf("| T_add    = %lf\n", times[T_ADD]);
    //printf("")
    printf("| T_sync   = %lf\n", times[T_SYNC]);
    printf("+-------------------------\n");
}