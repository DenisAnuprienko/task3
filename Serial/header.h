#ifndef HEADER_H
#define HEADER_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <time.h>

//#define USE_GPU

#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/transform.h>
#include <thrust/inner_product.h>

// MPI-related things

#define MASTER 0

enum TimeType
{
	T_NORM2 = 0,
	T_DOT,
	T_ADD,
	T_MATVEC,
	T_SYNC
};



// Task-based things

// Domain is [A1,A2]x[B1,b2]
#define A1 -3.0
#define A2  3.0
#define B1 -2.0
#define B2  2.0

class Partition;
class Solver;

// Vector is distributed

class Vec
{
private:
	thrust::device_vector<double> dvec;
	thrust::host_vector<double>   hvec;
	//double *hvec;

    size_t size;
    int H, W;
    double *vals;
    double *sendbuf_upper, *sendbuf_lower, *sendbuf_left, *sendbuf_right;
    double *recvbuf_upper, *recvbuf_lower, *recvbuf_left, *recvbuf_right;

    double hx, hy;
    int M, N;

	Partition *Prt;

public:
	Vec(Partition *P);
	~Vec();

	double & operator()(int i, int j)
	{
		#ifdef USE_GPU
		return hvec[(i+1)*H + j+1];
		#else
		return vals[(i+1)*H + j+1];
		#endif
	}

	Partition *getPrt(void) { return Prt; }

	void   Sync(void);                        // Send boundary values and receive ghost values from neighbors
	double DotProduct(Vec & w);               // (v, w)
	double Norm2(void);                       // ||v||_2
	void   MatVec(Vec & w);                   // v = Aw
	void   FormRHS(void);                     // v = RHS (of discretized Poisson equation)
    void   ComputeResidual(Vec & x, Vec & b); // v = Ax - b
    void   AddVec(Vec & w, double alpha);     // v = v + alpha*w

    // The same on GPU
    double DotGPU(Vec & v);
    double GPU_Norm2(void);
    double GPU_DotProduct(Vec & w);
    void   GPU_MatVec(Vec & w);
    void   GPU_AddVec(Vec & w, double alpha);

    void   GatherTo(double *g);               // Get data from all processes in one global vector
};


class Partition
{
private:
	int M, N;                      // mesh sizes
	int taskid, numtasks;          // task id and total number of MPI tasks
	int Left, Right, Upper, Lower; // ids of neighboring tasks (-1 if neighbor in this direction doesn't exist)
	int proc_x, proc_y;            // number of processes in each direction
	int idx, idy;                  // task indices in Cartesian grid
    int imin, imax, jmin, jmax;    // bounds of mesh indices in subdomain of this task
    int Height, Width;             // sizes of subdomain (number of mesh points)

    double times[5];

public:
	 Partition(int *argc_, char ***argv_, int m, int n);
	~Partition();
	int _M()      { return M;      }
	int _N()      { return N;      }
	int _taskid() { return taskid; }
	int _numtasks() { return numtasks; }
	int _Left()   { return Left;   }
	int _Right()  { return Right;  }
	int _Upper()  { return Upper;  }
	int _Lower()  { return Lower;  }
	int _imin()   { return imin;   }
	int _imax()   { return imax;   }
	int _jmin()   { return jmin;   }
	int _jmax()   { return jmax;   }
	int _Height() { return Height; }
	int _Width()  { return Width;  }
	int _proc_x() { return proc_x; }
	int _proc_y() { return proc_y; }

	void Neighbors(void);
	void Irange(void);
	void Jrange(void);

    //void Print_solution_VTK(Vec &v, Vec &procid, const char *filename);
    void Print_solution_VTK(double *v, const char *filename);
    void CheckError(double *x);

	double Time(void) { return clock(); }
	void   AddTime(time_t t, int i) { times[i] += (double)t/CLOCKS_PER_SEC; }
	void   PrintTimes(void);
};

class Solver
{
private:
	Partition *Prt;
	double atol, rtol;
	int maxit;
	double hx, hy;
	int dim;
	int M, N;

public:
	Solver(Partition *P, double at, double rt, int maxiter);
	~Solver();
	
	//void Solve(double *b, double *res);
	void Solve(Vec & b, Vec & res);
};


//void PrintVec(double *x, const char *filename);

double ExactSol(double x, double y);
double f(double x, double y);
double BC_Left(double y);
double BC_Top(double x);
double BC_Bottom(double x);
double BC_Corner(int cornernum);

#endif 
