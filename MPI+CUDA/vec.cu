#include "header.h"

struct square 
{ 
    __device__ __host__ double operator()(double x) const { return x*x; } 
};

struct addvec //: public thrust::binary_function<double, double, double>
{
    const double a;
    addvec(double d) : a(d) {}
    __host__ __device__
    double operator()(double x, double y) const
    {
        return x + a*y;
    }
};

struct mulvec //: public thrust::binary_function<double, double, double>
{
    mulvec() {}
    __host__ __device__
    double operator()(double x, double y) const
    {
        return x*y;
    }
};

struct matvec : public thrust::binary_function<double, int, double>
    {
        int imin, jmin, M, N;
        size_t H, W;
        double hx, hy;
        const double *v;
        matvec(thrust::device_vector<double> & vec, int imin_, int jmin_, int H_, int W_, int M_, int N_, double hX, double hY) : 
               v(thrust::raw_pointer_cast(vec.data())), 
               imin(imin_), jmin(jmin_), H(H_), W(W_), M(M_), N(N_), hx(hX), hy(hY)
        {
            //printf("i: [%d, %d], j: [%d, %d]\n", imin, imin+W-3, jmin, jmin+H-3);
        }
        __host__ __device__
        double operator()(const double & vij, const int & k) const
        {
            int i = k/H;                     // indices in vec
            int j = k - i*H;                 //
            // return hx;
            // i--;
            // j--;
            int I = i + imin - 1;                // indices in global domain
            int J = j + jmin - 1;                // 
            //printf("%2d (%2d,%2d)\n", k, I, J);

            double lft = 0.0, rgt = 0.0, upr = 0.0, lwr = 0.0;
            if(i > 0)
            //if(I > 0)
                lft = v[(i-1)*H + j];
            if(i < W-1)
            //if(I < M) 
                rgt = v[(i+1)*H + j];
            if(j < H-1)
            //if(J < N+1)
                upr = v[i*H + j+1];
            if(j > 0)
            //if(J > 0)
                lwr = v[i*H + j-1];

            double res;

            if(i == 0 || j == 0 || j == H-1 || i == W-1) {
                //printf(" Ghost\n");
                return 0e0;
                //res = 0e0;
            }

            // if(I == i + imin + W-3)//(I == 33 && imin != 33)
            //     printf("ERROR!!!!!!!!!\n");

            
            if(I == 0){ // Left side including corners
                if(J == 0){ // Left lower corner
                    //printf("L corner\n");
                    res = -2.0 * (rgt - vij) / (hx*hx) - 2.0 * (upr - vij) / (hy*hy);
                }
                else if(J == N){ // Left upper corner
                    //printf("U corner\n");
                    res = -2.0 * (rgt - vij) / (hx*hx) + 2.0 * (vij - lwr) / (hy*hy);
                }
                else{ // Left side excluding corners
                    //printf("L\n");
                    res = -2.0 * (rgt - vij) / (hx*hx) - (upr - 2.0*vij + lwr) / (hy*hy);
                }
            }
            else{
                if(J == 0 && I != 0){ // Bottom side
                     //printf("B\n");
                     if(I < M-1)
                         res = -(lft - 2.0*vij + rgt) / (hx*hx) - 2.0 * (upr - vij) / (hy*hy);
                     else{
                         //printf("(%d,%d): upr = %d\n", i, j, upr);
                         res = -(lft - 2.0*vij         ) / (hx*hx) - 2.0 * (upr - vij) / (hy*hy);
                     }
                }
                else if(J == N && I != 0){ // Top side
                     //printf("T\n");
                     if(I < M-1)
                         res = -(lft - 2.0*vij + rgt) / (hx*hx) + 2.0 * (vij - lwr) / (hy*hy);
                     else
                         res = -(lft - 2.0*vij           ) / (hx*hx) + 2.0 * (vij - lwr) / (hy*hy);
                }
                else{ // Interior points
                     if(I < M-1)
                         res = -(lft - 2.0*vij + rgt) / (hx*hx) - (upr - 2.0*vij + lwr) / (hy*hy);
                     else
                         res = -(lft - 2.0*vij         ) / (hx*hx) - (upr - 2.0*vij + lwr) / (hy*hy);
                }
            }
            return res;
        }

    };


double Vec::GPU_Norm2(void)
{
    //printf("GPU_Norm2()\n");

     // Make values at ghost points zeros
    // for(int i = 0; i < W; i++){
    //     hvec[i*H] = 0.0;
    //     hvec[(W-1)*H] = 0.0;
    // }
    // for(int j = 0; j < H; j++){
    //     hvec[j] = 0.0;
    //     hvec[(W-1)*H + j] = 0.0;
    // }

    //CopyToDevice();

    //printf("Norm: Copied to device successfully\n");

    //printf(" Copied\n");

    // size_t N = dvec.size();
    // thrust::device_vector<double> x_squared(N);
    //printf(" Created vec\n");
    // thrust::transform(dvec.begin(), dvec.end(), x_squared.begin(), square());
    // double norm = thrust::reduce(x_squared.begin(), x_squared.end());
    thrust::plus<double> pl;
    double norm = thrust::transform_reduce(dvec.begin(), dvec.end(), square(), 0.0, pl);
    //thrust::reduce(x_squared.begin(), x_squared.end());

    double dbuf = norm;    
    MPI_Allreduce(&dbuf, &norm, 1, MPI_DOUBLE, MPI_SUM, Prt->_Comm());

    return sqrt(norm);
}

double Vec::GPU_DotProduct(Vec & w)
{
    //printf("GPU_Norm2()\n");

     // Make values at ghost points zeros
    // for(int i = 0; i < W; i++){
    //     hvec[i*H] = 0.0;
    //     hvec[(W-1)*H] = 0.0;
    //     w.hvec[i*H] = 0.0;
    //     w.hvec[(W-1)*H] = 0.0;
    // }
    // for(int j = 0; j < H; j++){
    //     hvec[j] = 0.0;
    //     hvec[(W-1)*H + j] = 0.0;
    //     w.hvec[j] = 0.0;
    //     w.hvec[(W-1)*H + j] = 0.0;
    // }

    //CopyToDevice();//dvec = hvec;
    //w.CopyToDevice();//w.dvec = w.hvec;
    //printf("Dot: Copied to device successfully\n");

    //printf(" Copied\n");

    double prod = thrust::inner_product(dvec.begin(), dvec.end(), w.dvec.begin(), 0.0);

    //struct mul(alpha);
    // thrust::device_vector<double> prods = dvec;
    // thrust::transform(dvec.begin(), dvec.end(),  
    //                   w.dvec.begin(),
    //                   prods.begin(),
    //                   mulvec());

    // double prod = thrust::reduce(prods.begin(), prods.end());

    double dbuf = prod;    
    MPI_Allreduce(&dbuf, &prod, 1, MPI_DOUBLE, MPI_SUM, Prt->_Comm());

    return prod;
}

void Vec::GPU_MatVec(Vec & w)
{
    //printf("GPU_MatVec\n");

    // w.CopyToHost();
    // double t1, t2;
    // t1 = Prt->Time();
    // w.Sync();
    // t2 = Prt->Time();
    // Prt->AddTime(t2-t1, T_SYNC);
    // w.CopyToDevice();


    //CopyToDevice();//dvec = hvec;
    //w.CopyToDevice();//w.dvec = w.hvec;

    //printf("MatVec: Copied to device successfully\n");

    //printf("hx = %lf, hy = %lf\n", hx, hy);
    //printf("H = %d, W = %d\n", H, W);

    struct matvec mv(w.dvec, Prt->_imin(), Prt->_jmin(), H, W, M, N, hx, hy);
    //printf("Created matvec\n");
    thrust::transform(w.dvec.begin(), w.dvec.end(), 
                      thrust::make_counting_iterator(0), 
                      dvec.begin(),
                      mv);
    //printf("transform success\n");
    //CopyToHost();//hvec = dvec;
    //printf("MatVec completed successfully\n");
}

void Vec::GPU_AddVec(Vec & w, double alpha)
{
    //printf("GPU_MatVec\n");
    //CopyToDevice();//dvec = hvec;
    //w.CopyToDevice();//w.dvec = w.hvec;

    //printf("AddVec: Copied to device successfully\n");

    //printf("hx = %lf, hy = %lf\n", hx, hy);
    //printf("H = %d, W = %d\n", H, W);

    struct addvec s(alpha);
    //printf("Created matvec\n");
    thrust::transform(dvec.begin(), dvec.end(),  
                      w.dvec.begin(),
                      dvec.begin(),
                      s);
    //printf("transform success\n");
    //CopyToHost();//hvec = dvec;
}


// Sequence of actions at 1 iteration:

// rsd.ComputeResidual(res, b); --- A*res - b
        
// rsdnrm = rsd.Norm2();
// d.MatVec(rsd);
// tau = d.DotProduct(rsd);
// dnrm = d.Norm2();
// res.AddVec(rsd, -tau);
