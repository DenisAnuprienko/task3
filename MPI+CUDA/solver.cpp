#include "header.h"

Solver::Solver(Partition *P, double at, double rt, int maxiter)
{
    Prt = P;
    atol = at;
    rtol = rt;
    maxit = maxiter;

    M = Prt->_M();
    N = Prt->_N();

    dim = M*(N+1);
    hx = (A2 - A1) / M;
    hy = (B2 - B1) / N;
    if(P->_taskid() == MASTER)
        printf("Solver() completed\n");
}


Solver::~Solver()
{

}

void Solver::Solve(Vec & b, Vec & res)
{

    //double diffnrm = 2.0 * rtol;
    if(Prt->_taskid() == MASTER)
        printf("Solver: atol = %e, rtol = %e, maxit = %d\n", atol, rtol, maxit);

    if(Prt->_taskid() == MASTER){
        #ifdef USE_GPU
        printf("GPU enabled\n");
        #else
        printf("GPU disabled\n");
        #endif
    }
    
    double tau, rsdnrm, rsdnrm0, dnrm;

    int Height = Prt->_Height(), Width = Prt->_Width();

    Vec rsd(Prt);
    Vec   d(Prt);
    
    int i, j;
    int imin = Prt->_imin(), imax = Prt->_imax(), jmin = Prt->_jmin(), jmax = Prt->_jmax();
    
    double tbeg, tend, t1, t2;
    if(Prt->_taskid() == MASTER)
        tbeg = Prt->Time();

    t1 = Prt->Time();
    res.CopyToDevice();
    rsd.CopyToDevice();
    d.CopyToDevice();
    b.CopyToDevice();
    t2 = Prt->Time();
    Prt->AddTime(t2-t1, T_COPY);

    int iter;
    for(iter = 0; iter < maxit; iter++){

        // Computing residual (rsd) includes A*res, so we need to sync res
        // and for that we need to get it from device
        #ifdef USE_GPU
        t1 = Prt->Time();
        res.CopyToHost();
        t2 = Prt->Time();
        Prt->AddTime(t2-t1, T_COPY);

        t1 = Prt->Time();
        res.Sync();
        t2 = Prt->Time();
        Prt->AddTime(t2-t1, T_SYNC);
        
        t1 = Prt->Time();
        res.CopyToDevice();
        t2 = Prt->Time();
        Prt->AddTime(t2-t1, T_COPY);
        #endif

        rsd.ComputeResidual(res, b);
        t1 = Prt->Time();
        #ifdef USE_GPU
        rsdnrm = rsd.GPU_Norm2();
        #else
        rsdnrm = rsd.Norm2();
        #endif
        t2 = Prt->Time();
        Prt->AddTime(t2-t1, T_NORM2);
        //printf("First norm computed\n");

        // if(iter == 0)
        //     rsdnrm0 = rsdnrm;
        // if(rsdnrm < atol || rsdnrm < rtol * rsdnrm0)
        //     break;


        #ifdef USE_GPU
        t1 = Prt->Time();
        rsd.CopyToHost();
        t2 = Prt->Time();

        Prt->AddTime(t2-t1, T_COPY);
        t1 = Prt->Time();
        rsd.Sync();
        t2 = Prt->Time();
        Prt->AddTime(t2-t1, T_SYNC);

        t1 = Prt->Time();
        rsd.CopyToDevice();
        t2 = Prt->Time();
        Prt->AddTime(t2-t1, T_COPY);
        #endif

        t1 = Prt->Time();
        #ifdef USE_GPU
        d.GPU_MatVec(rsd);
        #else
        d.MatVec(rsd);
        #endif
        t2 = Prt->Time();
        Prt->AddTime(t2-t1, T_MATVEC);

        t1 = Prt->Time();
        #ifdef USE_GPU
        tau = d.GPU_DotProduct(rsd);
        #else
        tau = d.DotProduct(rsd);
        #endif
        t2 = Prt->Time();
        Prt->AddTime(t2-t1, T_DOT);

        if(iter == 0)
            rsdnrm0 = rsdnrm;
        if(rsdnrm < atol || rsdnrm < rtol * rsdnrm0)
            break;

        t1 = Prt->Time();
        #ifdef USE_GPU
        dnrm = d.GPU_Norm2();
        #else
        dnrm = d.Norm2();
        #endif
        //printf(" dnrm = %lf\n", dnrm);
        tau /= (dnrm * dnrm);
        t2 = Prt->Time();
        Prt->AddTime(t2-t1, T_NORM2);

        t1 = Prt->Time();
        #ifdef USE_GPU
        res.GPU_AddVec(rsd, -tau);
        #else
        res.AddVec(rsd, -tau);
        #endif
        t2 = Prt->Time();
        Prt->AddTime(t2-t1, T_ADD);
        // for(i = 0; i < Prt->_Width(); i++)
        //     for(j = 0; j < Prt->_Height(); j++)
        //         res(i,j) -= tau * rsd(i,j);
        
        if(iter%1000 == 0 && Prt->_taskid() == MASTER)
            printf("iter %d, |r| = %e, |r|/|r0| = %e\n", iter, rsdnrm, rsdnrm/rsdnrm0);

        //res.CopyToHost();
        //rsd.CopyToHost();
    }
    res.CopyToHost();

    //diffnrm = Norm2(dim, diff);
    
    if(Prt->_taskid() == MASTER){
        tend = Prt->Time();
        //printf("Solver: %d iters\n|r|             = %e\n|r|/|r0|        = %e\n|x_{k+1}-x_{k}| = %e\n", iter, rsdnrm, rsdnrm/rsdnrm0, diffnrm);
        printf("Solver: %d iters\n|r|             = %e\n|r|/|r0|        = %e\n", iter, rsdnrm, rsdnrm/rsdnrm0);
        printf("Time: %lf s\n", (tend-tbeg));
    }
}