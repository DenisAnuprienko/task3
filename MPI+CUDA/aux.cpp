#include "header.h"

double ExactSol(double x, double y)
{
    //return sin(M_PI*x / 6.0);
    //return x;
    return (x*x - 9.0)*(x*x - 9.0) + (y*y - 4.0)*(y*y - 4.0);
}

double f(double x, double y)
{
    //return M_PI*M_PI*sin(M_PI*x / 6.0) / 36.0;
    //return 0.0;
    return -12.0 * (x*x + y*y) + 52.0;
}

double BC_Left(double y)
{
    return 0.0;
}

double BC_Top(double x)
{
    return 0.0;
}

double BC_Bottom(double x)
{
    return 0.0;
}

double BC_Corner(int cornernum)
{
    if(cornernum == 0){ // Lower
        return 0.5 * (BC_Left(B1) + BC_Bottom(A1));
    }
    else{
        return 0.5 * (BC_Left(B2) + BC_Top(A1));
    }
}