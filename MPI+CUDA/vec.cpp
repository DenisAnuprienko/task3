#include "header.h"

Vec::Vec(Partition *P)
{
    Prt = P;

    M = Prt->_M();
    N = Prt->_N();

    hx = (A2 - A1) / M;
    hy = (B2 - B1) / N;

    H = Prt->_Height() + 2;
    W = Prt->_Width()  + 2;
    size = (size_t)(H * W);
    //printf("VEc: size = %d\n", size);

    hvec.resize((size_t)size);
    //dvec.resize((size_t)size);

    // vals = (double *)malloc(size * sizeof(double));
    // for(int i = 0; i < size; i++)
    //     vals[i] = 0.0;

    sendbuf_upper = (double *)malloc(Prt->_Width() * sizeof(double));
    sendbuf_lower = (double *)malloc(Prt->_Width() * sizeof(double));
    recvbuf_upper = (double *)malloc(Prt->_Width() * sizeof(double));
    recvbuf_lower = (double *)malloc(Prt->_Width() * sizeof(double));

    sendbuf_left  = (double *)malloc(Prt->_Height() * sizeof(double));
    sendbuf_right = (double *)malloc(Prt->_Height() * sizeof(double));
    recvbuf_left  = (double *)malloc(Prt->_Height() * sizeof(double));
    recvbuf_right = (double *)malloc(Prt->_Height() * sizeof(double));
    //printf("Vec constructed\n");
}

Vec::~Vec()
{
    //free(vals);
    free(sendbuf_upper);
    free(sendbuf_lower);
    free(recvbuf_upper);
    free(recvbuf_lower);
    free(sendbuf_left);
    free(sendbuf_right);
    free(recvbuf_left);
    free(recvbuf_right);
}

void Vec::Sync(void)
{

    MPI_Status status;

    if(Prt->_Left() != -1){
        // double *senddata, *recvdata;
        // senddata = vals + H + 1; // Beginning of meaningful values in our vector
        // recvdata = vals + 1;     // Beginning of ghost points section
        for(int j = 0; j < Prt->_Height(); j++)
            sendbuf_left[j] = (*this)(0,j);

        MPI_Send(sendbuf_left, Prt->_Height(), MPI_DOUBLE, Prt->_Left(), 0, Prt->_Comm());  
        MPI_Recv(recvbuf_left, Prt->_Height(), MPI_DOUBLE, Prt->_Left(), 0, Prt->_Comm(), &status);

        for(int j = 0; j < Prt->_Height(); j++)
            (*this)(-1,j) = recvbuf_left[j];
    }
    if(Prt->_Right() != -1){
        // double *senddata, *recvdata;
        // senddata = vals + (W-2)*H + 1;
        // recvdata = vals + (W-1)*H + 1;  
        for(int j = 0; j < Prt->_Height(); j++)
            sendbuf_right[j] = (*this)(Prt->_Width()-1, j);

        MPI_Recv(recvbuf_right, Prt->_Height(), MPI_DOUBLE, Prt->_Right(), 0, Prt->_Comm(), &status);  
        MPI_Send(sendbuf_right, Prt->_Height(), MPI_DOUBLE, Prt->_Right(), 0, Prt->_Comm());

        for(int j = 0; j < Prt->_Height(); j++)
            (*this)(Prt->_Width(), j) = recvbuf_right[j];
    }
    // In these cases we need to create buffers
    if(Prt->_Lower() != -1){
        for(int i = 0; i < Prt->_Width(); i++)
            sendbuf_lower[i] = (*this)(i,0);//vals[(1+i)*H + 1];

        MPI_Send(sendbuf_lower, Prt->_Width(), MPI_DOUBLE, Prt->_Lower(), 0, Prt->_Comm());     
        MPI_Recv(recvbuf_lower, Prt->_Width(), MPI_DOUBLE, Prt->_Lower(), 0, Prt->_Comm(), &status); 

        for(int i = 0; i < Prt->_Width(); i++)
            (*this)(i,-1) = recvbuf_lower[i];//vals[(1+i)*H + 0] = recvbuf_lower[i];
    }
    if(Prt->_Upper() != -1){
        for(int i = 0; i < Prt->_Width(); i++)
            sendbuf_upper[i] = (*this)(i, Prt->_Height()-1);//vals[(1+i)*H + H-2];

        MPI_Recv(recvbuf_upper, Prt->_Width(), MPI_DOUBLE, Prt->_Upper(), 0, Prt->_Comm(), &status); 
        MPI_Send(sendbuf_upper, Prt->_Width(), MPI_DOUBLE, Prt->_Upper(), 0, Prt->_Comm());   

        for(int i = 0; i < Prt->_Width(); i++)
            (*this)(i, Prt->_Height()) = recvbuf_upper[i];//vals[(1+i)*H + W-1] = recvbuf_upper[i];
    }
    //printf("Sync completed\n");
}

double Vec::DotProduct(Vec & w)
{
    double prod = 0e0;

    assert(Prt->_taskid() == w.getPrt()->_taskid());
    assert(size == w.size);


    for(int i = 0; i < W-2; i++)
        for(int j = 0; j < H-2; j++)
            prod += (*this)(i,j) * w(i,j);

    double dbuf = prod;    
    MPI_Allreduce(&dbuf, &prod, 1, MPI_DOUBLE, MPI_SUM, Prt->_Comm());

    return prod;
}

double Vec::Norm2(void)
{
    double norm = 0.0;

    for(int i = 0; i < W-2; i++)
        for(int j = 0; j < H-2; j++)
            norm += (*this)(i,j) * (*this)(i,j);

    double dbuf = norm;    
    MPI_Allreduce(&dbuf, &norm, 1, MPI_DOUBLE, MPI_SUM, Prt->_Comm());

    return sqrt(norm);
}

void Vec::MatVec(Vec & w)
{
    // if(Prt->_taskid() == MASTER)
    //     printf("MatVec\n");
    //Prt->SyncVec(w);
    double t1, t2;
    if(Prt->_numtasks() > 1){
        t1 = Prt->Time();
        w.Sync();
        t2 = Prt->Time();
        Prt->AddTime(t2-t1, T_SYNC);
    }

    // i and j are LOCAL indices for Vec, which is only
    // a subvector related to a subdomain
    // I and J are GLOBAL indices of mesh
    int I, J;
    int imin = Prt->_imin(), jmin = Prt->_jmin();
    //printf("MatVec: %d %d\n", W-2, H-2);

    for(int i = 0; i < W-2; i++){
        I = i + imin;
        for(int j = 0; j < H-2; j++){
    //  for(j = jmin; j <= jmax; j++){
    //      for(i = imin; i <= imax; i++){
            J = j + jmin;
            
            if(I == 0){ // Left side including corners
                if(J == 0){ // Left lower corner
                    //printf("L corner\n");
                    (*this)(i,j) = -2.0 * (w(i+1,j) - w(i,j)) / (hx*hx) - 2.0 * (w(i,j+1) - w(i,j)) / (hy*hy);
                }
                else if(J == N){ // Left upper corner
                    //printf("U corner\n");
                    (*this)(i,j) = -2.0 * (w(i+1,j) - w(i,j)) / (hx*hx) + 2.0 * (w(i,j) - w(i,j-1)) / (hy*hy);
                }
                else{ // Left side excluding corners
                    //printf("L\n");
                    (*this)(i,j) = -2.0 * (w(i+1,j) - w(i,j)) / (hx*hx) - (w(i,j+1) - 2.0*w(i,j) + w(i,j-1)) / (hy*hy);
                }
            }
            else{
                if(J == 0 && I != 0){ // Bottom side
                     //printf("B\n");
                     if(I < M-1)
                         (*this)(i,j) = -(w(i-1,j) - 2.0*w(i,j) + w(i+1,j)) / (hx*hx) - 2.0 * (w(i,j+1) - w(i,j)) / (hy*hy);
                     else{
                         //printf("(%d,%d): upr = %d\n", i, j, upr);
                         (*this)(i,j) = -(w(i-1,j) - 2.0*w(i,j)         ) / (hx*hx) - 2.0 * (w(i,j+1) - w(i,j)) / (hy*hy);
                     }
                }
                else if(J == N && I != 0){ // Top side
                     //printf("T\n");
                     if(I < M-1)
                         (*this)(i,j) = -(w(i-1,j) - 2.0*w(i,j) + w(i+1,j)) / (hx*hx) + 2.0 * (w(i,j) - w(i,j-1)) / (hy*hy);
                     else
                         (*this)(i,j) = -(w(i-1,j) - 2.0*w(i,j)         ) / (hx*hx) + 2.0 * (w(i,j) - w(i,j-1)) / (hy*hy);
                }
                else{ // Interior points
                     if(I < M-1)
                         (*this)(i,j) = -(w(i-1,j) - 2.0*w(i,j) + w(i+1,j)) / (hx*hx) - (w(i,j+1) - 2.0*w(i,j) + w(i,j-1)) / (hy*hy);
                     else
                         (*this)(i,j) = -(w(i-1,j) - 2.0*w(i,j)         ) / (hx*hx) - (w(i,j+1) - 2.0*w(i,j) + w(i,j-1)) / (hy*hy);
                }
            }
        }
    }
    // if(Prt->_taskid() == MASTER)
    //     printf("MatVec finished\n");
}   

void Vec::FormRHS(void)
{
    int I, J;
    int imin = Prt->_imin(), jmin = Prt->_jmin();


    for(int i = 0; i < W-2; i++){
        I = i + imin;
        for(int j = 0; j < H-2; j++){
            J = j + jmin;

            (*this)(i,j) = f(A1 + I*hx, B1 + J*hy);
            //continue;//////////////////////////////////////////////////////////////////////////////////////
            
            // Right boundary (Dirichlet) influence
            if(I == M-1){
               //printf("Right boundary\n");
               (*this)(i,j) += 1.0 / (hx*hx) * ExactSol(A2, B1 + J*hy);
            }
            
            //continue;//////////////////////////////////////////////////////////////////////////////////////
            
            if(I == 0){ // Left boundary
                if(J == 0){ // Left lower corner
                    (*this)(i,j) -= (2.0 / hx * BC_Left(B1) + 2.0 / hy * BC_Bottom(A1));// * BC_Corner(0);
                }
                else if(J == N){ // Left upper corner
                    (*this)(i,j) -= (2.0 / hx * BC_Left(B2) + 2.0 / hy * BC_Top(A1));// * BC_Corner(1);
                }
                else{ // The rest
                    (*this)(i,j) -= 2.0 / hx * BC_Left(B1 + J*hy);
                }
            }
            else if(J == 0 && I != 0){ // Bottom boundary excluding corner
               (*this)(i,j) -= 2.0 / hy * BC_Bottom(A1 + I*hx);
            }
            else if(J == N && I != 0){ // Upper boundary excluding corner
               (*this)(i,j) -= 2.0 / hy * BC_Top(A1 + I*hx);
            }
        }
    }
    //printf("FormRHS() completed\n");
}

void Vec::AddVec(Vec & w, double alpha)
{
    for(int i = 0; i < W-2; i++)
        for(int j = 0; j < H-2; j++)
            (*this)(i,j) += alpha*w(i,j);
}

void Vec::ComputeResidual(Vec & x, Vec & b)
{
    double t1, t2;
    t1 = Prt->Time();
    #ifdef USE_GPU
    GPU_MatVec(x);
    #else
    MatVec(x);
    #endif
    t2 = Prt->Time();
    Prt->AddTime(t2-t1, T_MATVEC);

    // for(int i = 0; i < W-2; i++)
    //     for(int j = 0; j < H-2; j++)
    //         (*this)(i,j) -= b(i,j);
    t1 = Prt->Time();
    #ifdef USE_GPU
    GPU_AddVec(b, -1.0);
    #else
    AddVec(b, -1.0);
    #endif
    t2 = Prt->Time();
    Prt->AddTime(t2-t1, T_ADD);
}

void Vec::GatherTo(double *g)
{
    int i, j;
    int _W, _H;

    MPI_Status status;

    MPI_Barrier(Prt->_Comm());

    printf("task %d reached GatherTo\n", Prt->_taskid());

    if(Prt->_taskid() == MASTER){
        double *recvdata;
        int id;
        int _imin, _imax, _jmin, _jmax, _W, _H;
        int iperproc_x = M / Prt->_proc_x();
        int iperproc_y = (N+1) / Prt->_proc_y(); 
        for(id = 1; id < Prt->_numtasks(); id++){
            int idx = id/Prt->_proc_y();
            _imin = idx*iperproc_x;
            if(idx == Prt->_proc_x()-1)
                _imax = M-1;
            else
                _imax = idx*iperproc_x + iperproc_x-1;

            _W = _imax - _imin + 1;

            int idy = id - idx*Prt->_proc_y();
            _jmin = idy*iperproc_y;
            if(idy == Prt->_proc_y()-1)
                _jmax = N;
            else
                _jmax = idy*iperproc_y + iperproc_y-1;

            _H = _jmax - _jmin + 1;

            recvdata = (double *)malloc(_W*_H * sizeof(double));


            MPI_Recv(recvdata, _W*_H, MPI_DOUBLE, id, 0, Prt->_Comm(), &status);

            for(i = 0; i < _W; i++){
                for(j = 0; j < _H; j++){
                    g[(_imin+i)*(N+1) + _jmin + j] = recvdata[i*_H + j];
                }
            }
            printf("Got data from task %d\n", id);


            free(recvdata);  
        }

        for(i = 0; i < Prt->_Width(); i++)
            for(j = 0; j < Prt->_Height(); j++)
                g[(Prt->_imin() + i)*(N+1) + Prt->_jmin()+j] = (*this)(i,j);
    }
    else{
        double *senddata = (double *)malloc(Prt->_Width()*Prt->_Height() * sizeof(double));
        for(i = 0; i < Prt->_Width(); i++){
            for(j = 0; j < Prt->_Height(); j++){
                senddata[i*Prt->_Height() + j] = (*this)(i, j);//v[(imin+i)*(N+1) + jmin + j];
            }
        }
        MPI_Send(senddata, Prt->_Height()*Prt->_Width(), MPI_DOUBLE, MASTER, 0, Prt->_Comm());
        free(senddata);
    }
}