#include "header.h"


int main(int argc, char *argv[])
{    
    if(argc < 6){
        printf("Args: M, N, atol, rtol, maxiter\n");
        return 1;
    }
    int M = atoi(argv[1]);
    int N = atoi(argv[2]);
    int dim = M*(N+1);
    double tol_a = (double)atof(argv[3]);
    double tol_r = (double)atof(argv[4]);
    int maxit = atoi(argv[5]);

    double *Gsol  = (double *)malloc(dim * sizeof(double));

    for(int i = 0; i < dim; i++)
        Gsol[i] = 0.0;

    Partition partition(&argc, &argv, M, N);

    // Create vectors, by default they are zeros
    Vec RHS(&partition);
    Vec Sol(&partition);

    // Set RHS
    RHS.FormRHS();

    // Sol.MatVec(RHS);
    // printf("norm after MatVec     = %lf\n", Sol.GPU_Norm2());
    // // for(int i = 0; i < M; i++)
    // //     for(int j = 0; j < N+1; j++)
    // //         Sol(i,j) = 1.0;
    // Sol.GPU_MatVec(RHS);
    // printf("norm after GPU_MatVec = %lf\n", Sol.GPU_Norm2());

    //printf("Set RHS\n");

    //RHS.Lol();

    //printf("norm = %lf\n", RHS.Norm2());

    //partition.FormRHS(rhs, proc);

    Solver solver(&partition, tol_a, tol_r, maxit);
    solver.Solve(RHS, Sol);

    partition.PrintTimes();

    //Sol.GatherTo(Gsol);

    //partition.Print_solution_VTK(Gsol, "sol.vtk");

    //partition.CheckError(Gsol);


    free(Gsol);
    //FreeData();
    
    return 0;
}



