#include "header.h"

Partition::Partition(int *argc_, char ***argv_, int m, int n)
{
    M = m;
    N = n;

    times[T_NORM2]  = 0.0;
    times[T_SYNC]   = 0.0;
    times[T_ADD]    = 0.0;
    times[T_MATVEC] = 0.0;
    times[T_DOT]    = 0.0;

    MPI_Init(argc_, argv_); 
    MPI_Comm_rank(MPI_COMM_WORLD, &taskid);
    MPI_Comm_size(MPI_COMM_WORLD, &numtasks);

    int dims[2] = {0, 0};
    int err = MPI_Dims_create(numtasks, 2, dims);
    if(err != MPI_SUCCESS)
        printf("Error %d occured in MPI_Dims_create()\n", err);


    proc_x = dims[0];
    proc_y = dims[1];

    if (taskid == MASTER){
        //printf("M = %d, N = %d, atol = %e, rtol = %e, maxit = %d\n", M, N, tol_a, tol_r, maxit);
        printf("M = %d, N = %d\n", M, N);
        printf("Number of MPI tasks:      %d\n", numtasks);
        #ifdef USE_OMP
        #pragma omp parallel
        {
            if(omp_get_thread_num() == 0)
                printf("Number of OpenMP threads: %d\n", omp_get_num_threads());
        }
        #else
        printf("OpenMP disabled\n");
        #endif
        printf("Grid: %d = %d * %d\n", numtasks, proc_x, proc_y);
        //printf("hx = %lf, hy = %lf\n", hx, hy);
    }

    int periods[2] = {0, 0};
    MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, 0, &Comm);

    MPI_Comm_size(Comm, &numtasks);
    MPI_Comm_rank(Comm, &taskid);

    Neighbors();
    Irange();
    Jrange();

    //idx = taskid/proc_y;
    //idy = taskid - idx*proc_y;
}

Partition::~Partition()
{
    MPI_Finalize();
}

void Partition::Neighbors(void)
{
    int cords[2];
    MPI_Cart_coords(Comm, taskid, 2, cords);
    idx = cords[0], idy = cords[1];

    if(idx > 0){ // Left
        int coords[2] = {idx-1, idy};
        MPI_Cart_rank(Comm, coords, &Left);
    } 
    else{
        Left = -1;
    }

    if(idx < proc_x-1){ // Right
        int coords[2] = {idx+1, idy};
        MPI_Cart_rank(Comm, coords, &Right);
    } 
    else{
        Right = -1;
    }

    if(idy > 0){ // Lower
        int coords[2] = {idx, idy-1};
        MPI_Cart_rank(Comm, coords, &Lower);
    } 
    else{
        Lower = -1;
    }

    if(idy < proc_y-1){ // Upper
        int coords[2] = {idx, idy+1};
        MPI_Cart_rank(Comm, coords, &Upper);
    } 
    else{
        Upper = -1;
    }


    //if(taskid == 1) printf("<%d >%d ^%d V%d\n", Left, Right, Upper, Lower);
}

void Partition::Irange(void)
{
    int iperproc = M / proc_x;
    //if(taskid == MASTER) printf("proc_x = %d, iperproc = %d\n", proc_x, iperproc);
    imin = idx*iperproc;
    if(idx == proc_x-1){
        imax = M-1;
    }
    else{
        imax = idx*iperproc + iperproc-1;
    }
    Width = imax - imin + 1;
    // printf("Task %d: Width = %d\n", taskid, Width);
    // printf("task %d, idx = %d, imin = %d, imax = %d\n", taskid, idx, imin, imax);
}

void Partition::Jrange(void)
{
    int iperproc = (N+1) / proc_y;
    // if(taskid == MASTER)
    //      printf("proc_y = %d, iperproc = %d\n", proc_y, iperproc);
    jmin = idy*iperproc;
    if(idy == proc_y-1){
        jmax = N;
    }
    else{
        jmax = idy*iperproc + iperproc-1;
    }
    Height = jmax - jmin + 1;
}

void Partition::Print_solution_VTK(double *x, double *procid, const char *filename)
{
    int i, j;

    double hx = (A2 - A1) / M;
    double hy = (B2 - B1) / N;

    if(taskid != MASTER)
        return;
    
    FILE *f = fopen(filename, "w");
    fprintf(f, "# vtk DataFile Version 3.0\n");
    fprintf(f, "\n");
    fprintf(f, "ASCII\n");
    fprintf(f, "DATASET RECTILINEAR_GRID\n");
    fprintf(f, "DIMENSIONS %d %d 1\n", M+1, N+1);
    
    
    fprintf(f, "X_COORDINATES %d double\n", M+1);
    for(i = 0; i <= M; i++)
        fprintf(f, "%lf\n", A1 + i*hx);
    fprintf(f, "\n");
    
    fprintf(f, "Y_COORDINATES %d double\n", N+1);
    for(i = 0; i <= N; i ++)
        fprintf(f, "%lf\n", i*hy);
    fprintf(f, "\n");
    
    fprintf(f, "Z_COORDINATES 1 double\n0\n\n");
    
    fprintf(f, "POINT_DATA %d\n", (M+1)*(N+1));
        
    fprintf(f, "SCALARS Numerical double\n"); 
    fprintf(f, "LOOKUP_TABLE default\n");
    for(j = 0; j <= N; j++){
    //for(i = 0; i <= M-1; i++){
        for(i = 0; i <= M-1; i++){
        //for(j = 0; j <= N; j++){
            fprintf(f, "%lf\n", x[i*(N+1)+j]);
        }
        fprintf(f, "%lf\n", ExactSol(A2, B1 + j*hy));
        //fprintf(f, "\n");
    }
    //for(j = 0; j <= N; j++)
    //    fprintf(f, "%lf\n", ExactSol(A2, B1 + j*hy));

    //for(i = 0; i < (M+1)*(N+1); i++)
    //    fprintf(f, "%d\n", i);
    
    fprintf(f, "\nSCALARS Exact double\n");
    fprintf(f, "LOOKUP_TABLE default\n");
    for(j = 0; j <= N; j ++){
    //for(i = 0; i <= M-1; i ++){
        //for(j = 0; j <= N; j ++){
        for(i = 0; i <= M; i ++){
            fprintf(f, "%lf ", ExactSol(A1 + i*hx, B1 + j*hy));
        }
        fprintf(f, "\n");
    }

    fprintf(f, "SCALARS TaskId double\n"); 
    fprintf(f, "LOOKUP_TABLE default\n");
    for(j = 0; j <= N; j++){
    //for(i = 0; i <= M-1; i++){
        for(i = 0; i <= M-1; i++){
        //for(j = 0; j <= N; j++){
            fprintf(f, "%lf\n", procid[i*(N+1)+j]);
        }
        fprintf(f, "-1\n");
        //fprintf(f, "\n");
    }
    printf("Finished printing solution\n");

    fclose(f);
}

// void Partition::Print_solution_VTK(Vec &x, Vec &procid, const char *filename)
// {
//     int i, j;

//     double hx = (A2 - A1) / M;
//     double hy = (B2 - B1) / N;

//     //GatherVec(x);
//     //GatherVec(procid);
//     MPI_Barrier(Comm);
//     if(taskid != MASTER)
//         return;
    
//     FILE *f = fopen(filename, "w");
//     fprintf(f, "# vtk DataFile Version 3.0\n");
//     fprintf(f, "\n");
//     fprintf(f, "ASCII\n");
//     fprintf(f, "DATASET RECTILINEAR_GRID\n");
//     fprintf(f, "DIMENSIONS %d %d 1\n", M+1, N+1);
    
    
//     fprintf(f, "X_COORDINATES %d double\n", M+1);
//     for(i = 0; i <= M; i++)
//         fprintf(f, "%lf\n", A1 + i*hx);
//     fprintf(f, "\n");
    
//     fprintf(f, "Y_COORDINATES %d double\n", N+1);
//     for(i = 0; i <= N; i ++)
//         fprintf(f, "%lf\n", i*hy);
//     fprintf(f, "\n");
    
//     fprintf(f, "Z_COORDINATES 1 double\n0\n\n");
    
//     fprintf(f, "POINT_DATA %d\n", (M+1)*(N+1));
        
//     fprintf(f, "SCALARS Numerical double\n"); 
//     fprintf(f, "LOOKUP_TABLE default\n");
//     for(j = 0; j <= N; j++){
//     //for(i = 0; i <= M-1; i++){
//         for(i = 0; i <= M-1; i++){
//         //for(j = 0; j <= N; j++){
//             fprintf(f, "%lf\n", x(i, j));//x[i*(N+1)+j]);
//         }
//         fprintf(f, "%lf\n", ExactSol(A2, B1 + j*hy));
//         //fprintf(f, "\n");
//     }
//     //for(j = 0; j <= N; j++)
//     //    fprintf(f, "%lf\n", ExactSol(A2, B1 + j*hy));

//     //for(i = 0; i < (M+1)*(N+1); i++)
//     //    fprintf(f, "%d\n", i);
    
//     fprintf(f, "\nSCALARS Exact double\n");
//     fprintf(f, "LOOKUP_TABLE default\n");
//     for(j = 0; j <= N; j ++){
//     //for(i = 0; i <= M-1; i ++){
//         //for(j = 0; j <= N; j ++){
//         for(i = 0; i <= M; i ++){
//             fprintf(f, "%lf ", ExactSol(A1 + i*hx, B1 + j*hy));
//         }
//         fprintf(f, "\n");
//     }

//     fprintf(f, "SCALARS TaskId double\n"); 
//     fprintf(f, "LOOKUP_TABLE default\n");
//     for(j = 0; j <= N; j++){
//     //for(i = 0; i <= M-1; i++){
//         for(i = 0; i <= M-1; i++){
//         //for(j = 0; j <= N; j++){
//             fprintf(f, "%lf\n", procid(i,j));//procid[i*(N+1)+j]);
//         }
//         fprintf(f, "-1\n");
//         //fprintf(f, "\n");
//     }
//     printf("Finished printing solution\n");

//     fclose(f);
// }


void Partition::CheckError(double *x)
{
    double newerr, err = 0.0;
    int i, j;

    double hx = (A2 - A1) / M;
    double hy = (B2 - B1) / N;

    if(taskid != MASTER)
        return;

    for(i = 0; i <= M-1; i++){
        for(j = 0; j <= N; j++){
            newerr =  fabs(x[i*(N+1)+j] - ExactSol(A1 + i*hx, B1 + j*hy));
            if(newerr > err)
                err = newerr;
        }
    }
    printf("Max error: %e\n", err); 
}

void Partition::PrintTimes(void)
{
    if(taskid != MASTER)
        return;

    printf("+-------------------------\n");
    printf("| T_norm   = %lf\n", times[T_NORM2]);
    printf("| T_dot    = %lf\n", times[T_DOT]);
    printf("| T_matvec = %lf\n", times[T_MATVEC]);
    printf("| T_add    = %lf\n", times[T_ADD]);
    //printf("")
    printf("| T_sync   = %lf\n", times[T_SYNC]);
    printf("+-------------------------\n");
}