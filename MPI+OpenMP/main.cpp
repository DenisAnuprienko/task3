#include "header.h"


int main(int argc, char *argv[])
{    
    if(argc < 6){
        printf("Args: M, N, atol, rtol, maxiter\n");
        return 1;
    }
    int M = atoi(argv[1]);
    int N = atoi(argv[2]);
    int dim = M*(N+1);
    double tol_a = (double)atof(argv[3]);
    double tol_r = (double)atof(argv[4]);
    int maxit = atoi(argv[5]);

    double *Gsol  = (double *)malloc(dim * sizeof(double));
    double *proc = (double *)malloc(dim * sizeof(double));

    for(int i = 0; i < dim; i++)
        Gsol[i] = 0.0;

    Partition partition(&argc, &argv, M, N);

    //printf("Created partition\n");

    // Create vectors, by default they are zeros
    Vec RHS(&partition);
    Vec Sol(&partition);

    //printf("Created vecs\n");

    // Set RHS
    RHS.FormRHS();

    //printf("Formed rhs\n");

    //printf("norm = %lf\n", RHS.Norm2());

    //partition.FormRHS(rhs, proc);

    Solver solver(&partition, tol_a, tol_r, maxit);
    //solver.Solve(rhs, sol);
    solver.Solve(RHS, Sol);

    partition.PrintTimes();

    Sol.GatherTo(Gsol);

    partition.Print_solution_VTK(Gsol, proc, "sol.vtk");

    partition.CheckError(Gsol);


    free(Gsol);
    free(proc);
    //FreeData();
    
    return 0;
}



